﻿using System;

namespace Moneybox.App
{
    public class Account
    {
        private decimal _balance;
        private decimal _withdrawn;
        private decimal _paidIn;

        public const decimal PayInLimit = 4000m;


        public Account(decimal balance, decimal withdrawn, decimal paidIn)
        {
            _balance = balance;
            _withdrawn = withdrawn;
            _paidIn = paidIn;
        }

        public Guid Id { get; set; }

        public User User { get; set; }

        public virtual decimal Balance { get { return _balance; } }

        public virtual decimal Withdrawn { get { return _withdrawn; } }

        public virtual decimal PaidIn { get { return _paidIn; } }

        public void UpdateBalances(decimal amount, bool isWithdrawn)
        {
            if (isWithdrawn)
            {
                _balance = _balance - amount;
                _withdrawn = _withdrawn - amount;
            }
            else 
            {
                _balance = _balance + amount;
                _paidIn = _paidIn + amount;
            }
        }

        public bool IsInsufficientFunds(decimal amount)
        {
            return _balance - amount < 0m;
        }

        public bool IsPaidInLimitExceeded(decimal amount)
        {
            return _paidIn + amount > PayInLimit;
        }

        public bool IsLowFundsNotificationRequired(decimal amount)
        {
            return _balance - amount < 500m;
        }

        public bool IsApproachingPayInLimitNotificationRequired(decimal amount)
        {
            return PayInLimit - (_paidIn + amount ) < 500m;
        }
    }
}
