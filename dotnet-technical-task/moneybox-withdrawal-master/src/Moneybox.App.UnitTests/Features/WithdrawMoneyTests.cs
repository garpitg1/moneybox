﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using NUnit.Framework;
using System;

namespace Moneybox.App.UnitTests.Features
{
    [TestFixture]
    public class WithdrawMoneyTests
    {
        private Mock<IAccountRepository> _accountRepositoryMock;
        private Mock<INotificationService> _notificationServiceMock;

        [SetUp]
        public void SetUp()
        {
            _accountRepositoryMock = new Mock<IAccountRepository>();
            _notificationServiceMock = new Mock<INotificationService>();
        }

        [Test]
        public void MoneyWithdrawl_Successful()
        {
            var id = Guid.NewGuid();

            var account = new Account(1000, 200, 0)
            { 
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);

            var withdrawMoney = new WithdrawMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);
            Assert.DoesNotThrow(() => withdrawMoney.Execute(id, 100));
        }

        [Test]
        public void MoneyWithdrawl_InsufficientFunds()
        {
            var id = Guid.NewGuid();

            var account = new Account(200, 200, 0)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);

            var withdrawMoney = new WithdrawMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);

            var ex = Assert.Throws<InvalidOperationException>(() => withdrawMoney.Execute(id, 300));
            Assert.That(ex.Message, Is.EqualTo("Insufficient funds to make transfer"));
        }

        [Test]
        public void MoneyWithdrawl_SuccessfulButLowFunds()
        {
            var id = Guid.NewGuid();

            var account = new Account(1000, 600, 0)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);
            _notificationServiceMock.Setup(x => x.NotifyFundsLow(It.IsAny<string>()));

            var withdrawMoney = new WithdrawMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);
            Assert.DoesNotThrow(() => withdrawMoney.Execute(id, 600));
        }
    }
}
