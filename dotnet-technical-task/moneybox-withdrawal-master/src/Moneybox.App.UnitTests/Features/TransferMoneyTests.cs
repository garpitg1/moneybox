﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Moneybox.App.UnitTests.Features
{

    [TestFixture]
    public class TransferMoneyTests
    {
        private Mock<IAccountRepository> _accountRepositoryMock;
        private Mock<INotificationService> _notificationServiceMock;

        [SetUp]
        public void SetUp()
        {
            _accountRepositoryMock = new Mock<IAccountRepository>();
            _notificationServiceMock = new Mock<INotificationService>();
        }

        [Test]
        public void TransferMoney_Successful()
        {
            var id = Guid.NewGuid();

            var account = new Account(1000, 200, 200)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);

            var transferMoney = new TransferMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);
            Assert.DoesNotThrow(() => transferMoney.Execute(id, id, 100));
        }

        [Test]
        public void TransferMoney_InsufficientFunds()
        {
            var id = Guid.NewGuid();

            var account = new Account(200, 200, 100)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);

            var transferMoney = new TransferMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);

            var ex = Assert.Throws<InvalidOperationException>(() => transferMoney.Execute(id, id, 300));
            Assert.That(ex.Message, Is.EqualTo("Insufficient funds to make transfer"));
        }

        [Test]
        public void TransferMoney_PayInLimitReached()
        {
            var id = Guid.NewGuid();

            var account = new Account(4000, 200, 4000)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);

            var transferMoney = new TransferMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);

            var ex = Assert.Throws<InvalidOperationException>(() => transferMoney.Execute(id, id, 300));
            Assert.That(ex.Message, Is.EqualTo("Account pay in limit reached"));
        }

        [Test]
        public void TransferMoney_SuccessfulButLowFunds_PayInLimitApproaching()
        {
            var id = Guid.NewGuid();

            var account = new Account(1000, 600, 3000)
            {
                Id = id,
                User = new User
                {
                    Id = id,
                    Name = "FirstName LastName",
                    Email = "firstname.lastname@moneybox.com"
                }
            };

            _accountRepositoryMock.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns(account);
            _notificationServiceMock.Setup(x => x.NotifyFundsLow(It.IsAny<string>()));
            _notificationServiceMock.Setup(x => x.NotifyApproachingPayInLimit(It.IsAny<string>()));

            var transferMoney = new TransferMoney(_accountRepositoryMock.Object, _notificationServiceMock.Object);
            Assert.DoesNotThrow(() => transferMoney.Execute(id, id, 600));
        }
    }
}

